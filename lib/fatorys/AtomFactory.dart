
import 'package:chemtris/enums/AtomType.dart';
import 'package:chemtris/models/Atom.dart';
import 'package:flutter/material.dart';

class AtomFactory{
  static Atom getAtom(AtomType type){
    switch(type){
      case AtomType.HYDROGEN:
        return Atom(symbol: "H", type: AtomType.HYDROGEN, colors: [Colors.blueAccent.withOpacity(0.9), Colors.lightBlueAccent.withOpacity(0.5)]);
    }
  }
}