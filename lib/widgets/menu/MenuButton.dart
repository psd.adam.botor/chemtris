
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MenuButton extends StatefulWidget {

  var onTapAction;

  var title;

  MenuButton(this.title,this.onTapAction);
  @override
  State<StatefulWidget> createState() => _MenuButtonState(onTapAction,title);
}

class _MenuButtonState extends State<MenuButton>{

  var onTapAction;

  var title;

  _MenuButtonState(this.onTapAction, this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      width: 156,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.lightBlueAccent, Colors.white, Colors.lightBlueAccent],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          stops: [0.1,0.5,0.9]
        ),
        borderRadius: BorderRadius.all(Radius.circular(12))
      ),
      child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Material(
              color: Colors.transparent,
              child: InkWell(
                highlightColor: Colors.transparent,
                splashColor: Colors.lightBlueAccent.withOpacity(0.3),
                borderRadius: BorderRadius.all(Radius.circular(12)),
                onTap: onTapAction,
                child: Container(
                  height: 48,
                  width: 156,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(12))
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(title,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                      ),)
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
    );
  }

}