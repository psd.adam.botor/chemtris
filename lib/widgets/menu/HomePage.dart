
import 'dart:io';

import 'package:chemtris/widgets/about_page/AboutPage.dart';
import 'package:chemtris/widgets/game_page/GamePage.dart';
import 'package:chemtris/widgets/menu/MenuButton.dart';
import 'package:chemtris/widgets/options_page/OptionsPage.dart';
import 'package:chemtris/widgets/pay_page/PayPage.dart';
import 'package:chemtris/widgets/share_page/SharePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(
              padding: EdgeInsets.only(bottom: 86),
              child: Text("Chemtris",
                style: TextStyle(
                  fontSize: 64,
                  color: Colors.lightBlueAccent,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic
                ),
              ),
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 16),
              child: MenuButton("Play Game",(){
                Navigator.push(context, MaterialPageRoute(builder: (context) => GamePage()));
              }),
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 16),
              child: MenuButton("About",(){
                Navigator.push(context, MaterialPageRoute(builder: (context) => AboutPage()));
              }),
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 16),
              child: MenuButton("Options",(){
                Navigator.push(context, MaterialPageRoute(builder: (context) => OptionsPage()));
              }),
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 16),
              child: MenuButton("Pay",(){
                Navigator.push(context, MaterialPageRoute(builder: (context) => PayPage()));
              }),
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 16),
              child: MenuButton("Share",(){
                Navigator.push(context, MaterialPageRoute(builder: (context) => SharePage()));
              }),
            ),

            Padding(
              padding: EdgeInsets.only(bottom: 16),
              child: MenuButton("Exit",(){
                exit(0);
              }),
            )
          ],
        ),
      ),
    );
  }
}