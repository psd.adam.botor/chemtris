

import 'dart:math';

import 'package:chemtris/models/Atom.dart';
import 'package:flutter/material.dart';

class AtomWidget extends StatefulWidget {

  Atom atom;

  AnimationController controller;

  _AtomState state;

  AtomWidget(this.atom){
    state = _AtomState(atom);
    controller = AnimationController(
      duration: const Duration(seconds: 5),
      vsync: state,
    );
  }
  @override
  State<AtomWidget> createState() {
    return state ?? _AtomState(atom);
  }

  void moveRight(){
    state.moveRight();
  }

  void moveLeft(){
    state.moveLeft();
  }

  void stopMoving(){
    state.stopMoving();
  }
}

class _AtomState extends State<AtomWidget> with TickerProviderStateMixin {

  Atom atom;

  _AtomState(this.atom);

  var wasInit = false;

  var random = Random();
  var rightPosition = 100.0;

  Animation<double> rightAnimation;

  Animation<double> animation;

  AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(
      duration: const Duration(seconds: 4),
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    widget.controller.dispose();
    super.dispose();
  }

  void _initPosition(){
    double max = MediaQuery.of(context).size.width - 64;
    var newPosition = random.nextInt(max.round()) * 1.0;
    rightPosition = newPosition;
  }

  void moveRight(){
    setState(() {
      rightAnimation = Tween<double>(begin: rightPosition, end: MediaQuery.of(context).size.width - 64).animate(CurvedAnimation(parent: _animationController,  curve: Curves.decelerate));
      _animationController.forward(from: 0.0);
    });
  }

  void stopMoving(){
    _animationController.stop();
    rightPosition = rightAnimation.value;
  }

  void moveLeft(){
    setState(() {
      rightAnimation = Tween<double>(begin: rightPosition, end: 64).animate(CurvedAnimation(parent: _animationController,  curve: Curves.decelerate));
      _animationController.forward(from: 0.0);
    });
  }

  @override
  Widget build(BuildContext context) {
    if(!wasInit){
      _initPosition();
      animation = Tween<double>(begin: -64.0, end: MediaQuery.of(context).size.height - 64).animate(CurvedAnimation(parent: widget.controller,  curve: Curves.bounceOut));
      wasInit = true;
    }

    return AnimatedBuilder(
        animation: animation,
        builder: (context,widget){
          return Positioned(
              top: animation.value,
              left: rightAnimation?.value ?? rightPosition,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: _drawAtom()
                  ),
                ],
              ));
        });
  }

  Widget _drawAtom(){
    return Container(
      width: 64,
      height: 64,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: atom.colors),
          shape: BoxShape.circle
      ),

      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(atom.symbol,
            style: TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.bold
            ),)
        ],
      ),
    );
  }

}