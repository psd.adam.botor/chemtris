
import 'dart:math';

import 'package:chemtris/enums/AtomType.dart';
import 'package:chemtris/fatorys/AtomFactory.dart';
import 'package:chemtris/widgets/widget/atom_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class GamePage extends StatefulWidget {
  GamePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _GamePageState createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> with SingleTickerProviderStateMixin {

  List<AtomWidget> list = [AtomWidget(AtomFactory.getAtom(AtomType.HYDROGEN))];

  AnimationController _controller;

  void _initController(){
    _controller = list.last.controller;

    _controller.forward();

    _controller.addStatusListener((status){
      if(status == AnimationStatus.completed){
        setState(() {
          list.add(AtomWidget(AtomFactory.getAtom(AtomType.HYDROGEN)));
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    _initController();
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Positioned(
            child: Stack(
              children: list,
            ),
          ),

          Positioned(
            top: MediaQuery.of(context).size.height - 128,
            left: 64,
            child: GestureDetector(
              onTapDown: (details){
                list.last.moveLeft();
              },
              onTapUp: (details){
                list.last.stopMoving();
              },
              child: Icon(Icons.arrow_back_ios,
                color: Colors.blueAccent,
                size: 48,
              ),
            ),
          ),

          Positioned(
            top: MediaQuery.of(context).size.height - 128,
            left: MediaQuery.of(context).size.width - 112,
            child: GestureDetector(
              onTapDown: (details){
                list.last.moveRight();
              },
              onTapUp: (details){
                list.last.stopMoving();
              },
              child: Icon(Icons.arrow_forward_ios,
                  color: Colors.blueAccent,
                size: 48,
              ),
            ),
          )
        ],
      )
    );
  }
}