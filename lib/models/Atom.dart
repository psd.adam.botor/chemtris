
import 'dart:ui';

import 'package:chemtris/enums/AtomType.dart';

class Atom{

  String symbol;

  AtomType type;

  List<Color> colors;

  Atom({this.symbol, this.type, this.colors});
}